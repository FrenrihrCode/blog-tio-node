const express = require('express');
const Nota = require('../models/inventario');
const Usuario = require('../models/usuario');
const router = express.Router();

router.get('/notas', async function(req, res){
    let error = [];
    const notas = await Nota.find({});
    const usersName = await Usuario.find({});
    res.render('tareas', {error, notas, usersName })
});

router.get('/editNote/:id', async function(req, res){
    let error = [];
    const usersName = await Usuario.find({});
    const nota = await Nota.findById(req.params.id);
    res.render('editNote', {error, nota, usersName })
});

router.post('/newNota',  async function(req, res) {
    let error = [];
    const { titulo, descripcion, user } = req.body;
    const errors = [];
    if (!titulo) {
        errors.push({ text: "Título es requerido." });
    }
    if (!descripcion) {
        errors.push({ text: "Descripción es requerido." });
    }
    if (errors.length > 0) {
        res.render("tareas", {
            error,
        });
    } else {
        let newNota = new Nota({ title: titulo, description: descripcion, user: user, });
        await newNota.save();
        res.redirect("/notas");
    }
});

router.put('/updateNota/:id', async function(req, res){
    const { titulo,  descripcion, user } = req.body;
    await Nota.findByIdAndUpdate(req.params.id, { title: titulo, description: descripcion, user: user });
    res.redirect("/notas");
})

router.delete('/deleteNota/:id', async function(req, res) {
    let id = req.params.id;
    await Nota.findByIdAndDelete(id);
    res.redirect("/notas");
});
module.exports = router