const express = require('express');
const bcrypt = require('bcrypt');
const Usuario = require('../models/usuario');
const router = express.Router();

router.get('/', function(req, res){
    res.render('home')
});
router.get('/login', function(req, res){
    let error = [];
    res.render('login', {error, 
        username: '',
        email: '',
        password: '',
        confirmPassword: ''})
});
router.get('/register', function(req, res){
    let error = [];
    res.render('register', {error, 
        username: '',
        email: '',
        password: '',
        confirmPassword: ''})
});
router.get('/welcome', function(req, res){
    res.render('welcome')
});
router.post('/registrar',  async function(req, res) {
    let error = [];
    const { username, email, password, confirmPassword } = req.body;
    if (password != confirmPassword) {
        error.push({ text: "Contraseñas no coinciden." });
        res.render("register", {
            error,
            username,
            email,
            password,
            confirmPassword
        });
    } else {
        // Look for email coincidence
        const emailUser = await Usuario.findOne({ email: email });
        const nameUser = await Usuario.findOne({ nombre: username });
        if (emailUser) {
            error.push({ text: "El correo ya esta en uso." });
        }
        if (nameUser) {
            error.push({ text: "El usuario ya esta en uso." });
        }
        if(error.length >0 ){
            res.render("register", {
                error,
                username,
                email,
                password,
                confirmPassword
            });
        } else {
          // Saving a New User
          const newUser = new Usuario({ nombre:username, email:email, password:password });
          newUser.password = await newUser.encryptPassword(password);
          await newUser.save();
          res.redirect("/welcome");
        }
    }
    
});
router.post('/ingresar', async function(req, res) {
    let error = [];
    const { email, password } = req.body;
    const user = await Usuario.findOne({email: email});
    if (!user) {
        error.push({ text: "Credenciales inválidas." });
        res.render("login", {
            error,
            email,
            password
        });
    } else {
        console.log(password);
        const match = await user.matchPassword(password);
        if(match) {
            res.render('welcome')
        } else {
            error.push({ text: "Credenciales inválidas." });
            res.render("login", {
                error,
                email,
                password
            });
        }
        
    }
});
//listar
router.get('/usuario', function(req, res) {
    Usuario.find({}).exec((err, usuarios) =>{
        if(err) {
            return res.status(400).json({
                ok: false,
                err,
            });
        }
        res.json({
            ok: true,
            usuarios
        });
    });
});

//insertar
router.post('/usuario', function (req, res) {
    let body = req.body;
    let usuario = new Usuario({
        nombre: body.nombre,
        email: body.email,
        password: bcrypt.hashSync(body.password, 10),
        role: body.role,
    });

    usuario.save((err, usuarioDB) => {
        if(err) {
            return res.status(400).json({
                ok: false,
                err,
            });
        }
        usuarioDB.password = null;
        res.json({
            ok: true,
            usuario: usuarioDB,
        });
    });
});

//actualizar
router.put('/usuario/:id',function(req, res) {
    let id = req.params.id;
    let body = req.body;
    Usuario.findByIdAndUpdate(id, body, { new: true }, (err, usuarioDB) => {
        if(err){
            return res.status(400).json({
                ok: false,
                err,
            });
        }
        res.status(200).json({
            ok: true,
            usuario: usuarioDB,
        });
    });
});

//borrar
router.delete('/usuario/:id',function(req, res) {
    let id = req.params.id;

    Usuario.findByIdAndRemove(id, (err, usuarioBorrado) => {
        if(err){
            return res.status(400).json({
                ok: false,
                err,
            });
        }
        res.json({
            ok: true,
            usuario: usuarioBorrado,
        });
    });
});

module.exports = router