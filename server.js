const express = require('express');
const mongoose = require('mongoose');
const app = express();
const routesU = require('./routes/routesUser');
const routesN = require('./routes/routesNotes');
const methodOverride = require('method-override');

const port = process.env.PORT || 3000

app.use(express.json());
app.use(express.urlencoded());

mongoose.connect('mongodb://localhost:27017/lab07DB', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  }, (err, des) =>{
    if(err) throw err;
    console.log('Base de datos online')
});

app.use(express.static(__dirname + '/public'));
app.set('view engine', 'pug');
app.use(methodOverride('_method'));
app.listen(port, () => console.log(`Escuchando puerto ${port}`));

app.use(routesU);
app.use(routesN);

app.use(function(req, res, next) {
    res.status(404).sendFile(process.cwd() + '/app/views/404');
});